﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _09_26_2018_
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> st = Student.CreateRandomStudents(13).ToList();

            string path = @"C:\Testdir\Student.txt";
            if (File.Exists(path))
                File.WriteAllText(path, String.Empty);
            TextWriter tw = new StreamWriter(path);

            foreach (Student stud in st)
            {
                tw.WriteLine($"{stud.name}, {stud.surname}, {stud.phoneNumber}");
            }
            tw.Close();
            //if (!File.Exists(path))
            //{
            //    File.Create(path);
            //    TextWriter tw = new StreamWriter(path);
            //    foreach (Student stud in st)
            //    {
            //        tw.WriteLine($"{stud.name}, {stud.surname}, {stud.phoneNumber}");
            //    }
            //    tw.Close();
            //}
            //else if (File.Exists(path))
            //{
            //    File.WriteAllText(path, String.Empty);
            //    TextWriter tw = new StreamWriter(path);

            //    foreach (Student stud in st)
            //    {
            //        tw.WriteLine($"{stud.name}, {stud.surname}, {stud.phoneNumber}");
            //    }
            //    tw.Close();
            //}
        }
    }
}
